#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

void heapsort(vector<int> V)
{
    int End=V.size()-1, j, key, P=1, O=1, K=1;
    while(End)
    {
        P++;
        if(End%2!=0)
        {
            K++;
            if(V[End]>(V[End/2]))
            {
                swap(V[End],(V[End/2]));
                O++;
            }
            j=(End-1);
        }
        else
        {
            j=End;
        }
        while(j)
        {

            key=j;
            K++;
            if(V[key]<V[j-1])
            {
                key=j-1;
            }
            if(V[key]>V[(j-1)/2])
            {
                swap(V[key],V[(j-1)/2]);
                O++;
            }
            j-=2;
        }
        swap(V[0],V[End]);
        End--;
    }

    cout<<"��������������� ��c��������������� ������������� �����������:"<<endl;
    for(int i=0;i<V.size();i++)
    {
        cout<<V[i]<<' ';
    }
    cout<<endl;

    cout<<"P="<<P<<' '<<"O="<<O<<' '<<"K="<<K<<endl;
}

vector<int> quicksort(vector<int> V, int first, int last)
{
    int b, e, m;
    m=(first+last)/2;
    b=first;
    e=last;
    while(b<e)
    {
        while(V[b]<V[m])
        {
            b++;
        }
        while(V[e]>V[m])
        {
            e--;
        }
        if(b<=e)
        {
            swap(V[b],V[e]);
            b++;
            e--;
        }
        if(e>first)
        {
            V=quicksort(V, first, e);
        }
        if(b<last)
        {
            V=quicksort(V, b, last);
        }
    }

    return (V);
}

int main()
{
    setlocale(LC_ALL,"Russian");
    int N;
    cout<<"������� ���������� ��������� �����: ";
    cin>>N;
    vector<int> V;
    for (int i=0;i<N;i++)
    {
        V.push_back(rand()%1000);
    }
    cout<<"��������� ������������������:"<<endl;
    for(int i=0;i<N;i++)
    {
        cout<<V[i]<<' ';
    }
    cout<<endl;
    //cout<<V[V.size()-1];
    heapsort(V);
    V=quicksort(V, 0, N-1);

    cout<<"��������������� ������������������ ������� �����������:"<<endl;
    for(int i=0;i<N;i++)
    {
        cout<<V[i]<<' ';
    }
    cout<<endl;
}
