//����� ���� (0.5,1.5)
#include <iostream>
#include <math.h>

using namespace std;

double funk(double x)
{
    return (3*x-exp(x));
}

double der(double x)
{
   return ((-1)*exp(x));
}

void Find(double a, double b)
{
    double c, x0, x1, E=0.001;
    c=(a+b)/2;
    if(funk(a)*der(c)>0)
    {
        x1=b;
        do
        {
            x0=x1;
            x1=x0-(funk(x0)*(x0-a)/(funk(x0)-funk(a)));
            cout<<x1<<' ';
        }
        while(fabs(x0-x1)>E);
    }
    else
    {
        x1=a;
        do
        {
            x0=x1;
            x1=x0-(funk(x0)*(b-x0)/(funk(b)-funk(x0)));
            cout<<x1<<' ';
        }
        while(fabs(x0-x1)>E);
    }
    //return x1;
}

int main()
{
    double a=0, b=1;
    Find(a,b);
    return 0;
}
