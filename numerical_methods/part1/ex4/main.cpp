//����� ������� �������� (0,1)
#include <iostream>
#include <math.h>

using namespace std;

double fi(double x)
{
    return (sin(sqrt(1-0.4*x*x)));
}

double fid(double x)
{
    return (cos(sqrt(1-0.4*x*x)*0.8*x/2*sqrt(1-0.4*x*x)));
}

int main()
{
    double a=0, b=1, x0, x1, E=0.00001;
    int k=0;
    x0=(a+b)/2;
    if(fid(x0)<1)
    {
        x1=fi(x0);
        while(fabs(x1-x0)>E)
        {
            x0=x1;
            x1=fi(x0);
            k++;
        }
    }
    cout<<x1<<endl;
    cout<<k;
    return 0;
}
