#include <iostream>
#include <math.h>
#include <vector>
#include <algorithm>

using namespace std;

int main()
{
    double x, E=0.0001, help=0, a, mx1=0;
    vector<double>V, X0, X1;
    vector< vector<double> >M;
    int k=0;
    while(cin>>x)
    {
        V.push_back(x);
        if(cin.peek()=='\n')
        {
            M.push_back(V);
            V.clear();
        }
    }
    for(int i=0;i<M.size();i++)
    {
        for(int j=0;j<M[i].size();j++)
        {
            if(i!=j)
            {
                M[i][j]/=M[i][i]*(-1);
            }
        }
        M[i][i]=0;
        M[i][M.size()]*=(-1);
    }
    for(int i=0;i<M.size();i++)
    {
        for(int j=0;j<M[i].size();j++)
        {
            cout<<M[i][j]<<' ';
        }
        cout<<endl;
    }
    cout<<endl;
    for(int i=0;i<M.size();i++)
    {
        X1.push_back(M[i][M.size()]);
        X0.push_back(0);
    }
    /*for(int i=0;i<M.size();i++)
    {
        cout<<X1[i]<<' ';
    }*/
    do
    {
        mx1=0;
        for(int i=0;i<M.size();i++)
        {
            X0[i]=X1[i];
        }
        for(int i=0;i<M.size();i++)
        {
            for(int j=0;j<M.size();j++)
            {
                help+=M[i][j]*X1[j];
            }
            help+=M[i][M.size()];
            X1[i]=help;
            help=0;
            a=fabs(X1[i]-X0[i]);
            mx1=max(fabs(mx1),fabs(a));
        }
        k++;
    }
    while((fabs(mx1))>E);
    cout<<"k="<<k<<endl<<endl;
    for(int i=0;i<M.size();i++)
    {
        cout<<"x["<<i+1<<"]="<<X1[i]<<endl;
    }
    return 0;
}
/*
10 2 6 2.8
1 10 9 7
2 -7 -10 -17
*/
