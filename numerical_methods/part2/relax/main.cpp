#include <iostream>
#include <math.h>
#include <vector>

using namespace std;

int main()
{
    double x;
    vector<double>V;
    vector< vector<double> >M;
    while(cin>>x)
    {
        V.push_back(x);
        if(cin.peek()=='\n')
        {
            M.push_back(V);
            V.clear();
        }
    }

    for(int i=0;i<M.size();i++)
    {
        if(M[i][i]==0)
        {
            swap(M[i],M[i+1]);
        }
    }

    for(int i=0;i<M.size();i++)
    {
        for(int j=0;j<M.size();j++)
        {
            M[i][j]/=M[i][i]*(-1);
        }
    }

    for(int i=0;i<M.size();i++)
    {
        for(int j=0;j<M[i].size();j++)
        {
            cout<<M[i][j]<<' ';
        }
        cout<<endl;
    }
    return 0;
}

/*
10 2 6 2.8
1 10 9 7
2 -7 -10 -17
*/
