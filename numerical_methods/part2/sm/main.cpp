#include <iostream>
#include <vector>
#include <math.h>
#include <fstream>

using namespace std;

void calc_y1 (vector <vector <double> > &M, vector <double> &V1, vector <double> &V2)
{
    double p;
    V2.clear();
    for (int i=0;i<M.size();i++)
    {
        p=0;
        for (int j=0;j<M.size();j++)
        {
            p+=M[i][j]*V1[j];
        }
        V2.push_back(p);
    }
}

double calc_l2(vector <double> &V1, vector <double> &V2)
{
    double n=0,d=0;
    for (int i=0;i<V1.size();i++)
    {
        n+=V1[i]*V2[i];
        d+=V2[i]*V2[i];
    }
    return n/d;
}

double f(double &a, double &b)
{
    return fabs((b-a)/a);
}

double m (vector <double> &V)
{
    double m=V[0]*V[0];
    for (int i=1;i<V.size();i++)
    {
        m+=V[i]*V[i];
    }
    m=sqrt(m);
    return m;
}

int main()
{
    ifstream F("in.txt");
    ofstream Fout("out.txt");
    if(F)
    {
        vector <vector <double> > M;
        vector <double> y0, y1;
        double x, E=0.0001;
        while (F>>x)
        {
            y0.push_back(x);
            if (F.peek()=='\n')
            {
                M.push_back(y0);
                y0.clear();
            }
        }
        for (int i=0;i<M.size();i++)
        {
            y0.push_back(1);
        }
        double l1,l2;
        calc_y1(M,y0,y1);
        l2=calc_l2(y1,y0);

        do
        {
            y0=y1;
            calc_y1(M,y0,y1);
            l1=l2;
            l2=calc_l2(y1,y0);
        }
        while(f(l1,l2)>E);

        cout<<"l[n]="<<l2<<endl<<endl;
        Fout<<l2<<endl<<endl;

        for(int i=0;i<y1.size();i++)
        {
            cout<<"x["<<i+1<<"]="<<y1[i]/m(y1)<<endl;
            Fout<<y1[i]/m(y1)<<endl;
        }
    }
    else
    {
        cout<<"Error!!!";
    }
    F.close();
    Fout.close();
    return 0;
}
