#include <iostream>

using namespace std;

struct Data
{
    int a;
};
struct List
{
    Data d;
    List *next;
};

void Init(List **Begin)
{
    cout<<"Vvedite spisok:"<<endl;
    int N;
    cin>>N;
    *Begin=new List;
    (*Begin)->d.a=N;
    (*Begin)->next=NULL;
    List *End=*Begin;
    while(cin>>N)
    {
        End->next=new List;
        End=End->next;
        End->d.a=N;
        End->next=NULL;
    }
}

void Print(List *u)
{
    List *p=u;
    while(p)
    {
        cout<<p->d.a<<" ";
        p=p->next;
    }
}

void F(List *u, int N1, int N2)
{
    List *p=u;
    int E;
    while(p)
    {
        if(p->next->next==NULL)
        {
            E=p->next->d.a;
            p=p->next;
            p->d.a=N1;
            p->next=new List;
            p=p->next;
            p->d.a=N2;
            p->next=new List;
            p=p->next;
            p->d.a=E;
            break;
        }
        else
            p=p->next;
    }
}

int main()
{
    int N1, N2;
    cout<<"Elementi:"<<endl;
    cin>>N1>>N2;
    List *Begin=NULL;
    Init(&Begin);
    F(Begin,N1,N2);
    Print(Begin);
    return 0;
}
