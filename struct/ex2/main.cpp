#include <iostream>

using namespace std;

struct Data
{
    int a;
};
struct List
{
    Data d;
    List *next;
};
void Init(List **Begin)
{
    cout<<"Vvedite spisok:"<<endl;
    int N;
    *Begin=new List;
    (*Begin)->next=NULL;
    List *End=*Begin;
    while(cin>>N)
    {
        End->next=new List;
        End=End->next;
        End->d.a=N;
        End->next=NULL;
    }
}
void Print(List *u)
{
    List *p=u;
    p=p->next;
    while(p)
    {
        cout<<p->d.a<<" ";
        p=p->next;
    }
}
void Delete(List **u,Data &x)
{
    if(*u==NULL)
    {
        return;
    }
    List *t=*u;
    List *t1=t->next;
    while(t1)
    {
        if(t1->d.a==x.a)
        {
            t->next=t1->next;
            delete t1;
            t1=t->next;
        }
        else
        {
            t=t->next;
            t1=t->next;
        }
        //t=t1;
        //t1=t->next;
    }
}
void F(List *u)
{
    List *p=u;
    while(p->next)
    {
        if(p->d.a==p->next->d.a)
        {
            Delete(&u,p->d);
            Print(u);
            cout<<endl;
        }
        p=p->next;
    }
}

int main()
{
    int N1, N2;
    List *Begin=NULL;
    Init(&Begin);
    F(Begin);
    Print(Begin);
    return 0;
}
