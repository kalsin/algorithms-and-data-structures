#include <iostream>

using namespace std;

struct Data
{
    int a;
};
struct List
{
    Data d;
    List *next;
};
void Init(List **Begin1, List **Begin2)
{
    int N;
    *Begin1=new List;
    (*Begin1)->next=NULL;
    List *End1=*Begin1;

    *Begin2=new List;
    (*Begin2)->next=NULL;
    List *End2=*Begin2;

    while(cin>>N)
    {
        if(cin.peek()!='\n')
        {
            End1->next=new List;
            End1=End1->next;
            End1->d.a=N;
            End1->next=NULL;
        }
        else
        {
            End1->next=new List;
            End1=End1->next;
            End1->d.a=N;
            End1->next=NULL;
            break;
        }
    }
    while(cin>>N)
    {
        End2->next=new List;
        End2=End2->next;
        End2->d.a=N;
        End2->next=NULL;
    }
}
void F(List *u1, List *u2)
{
    List *p1=u1, *p2=u2;
    p1=p1->next;
    while(p1->next)
    {
        p1=p1->next;
    }
    p1->next=p2->next;
}
void Print(List *u)
{
    List *p=u;
    p=p->next;
    while(p)
    {
        cout<<p->d.a<<" ";
        p=p->next;
    }
}


int main()
{
    int N1, N2;
    List *Begin1=NULL;
    List *Begin2=NULL;
    Init(&Begin1,&Begin2);
    Print(Begin1);
    cout<<endl;
    Print(Begin2);
    cout<<endl;
    F(Begin1, Begin2);
    cout<<endl;
    Print(Begin1);
    return 0;
}
