#include <iostream>

using namespace std;

struct Data
{
    int a;
};
struct List
{
    Data d;
    List *next;
};

void Init(List **Begin1, List **Begin2)
{
    int N;
    *Begin1=new List;
    (*Begin1)->next=NULL;
    List *End1=*Begin1;

    *Begin2=new List;
    (*Begin2)->next=NULL;
    List *End2=*Begin2;

    while(cin>>N)
    {
        if(cin.peek()!='\n')
        {
            End1->next=new List;
            End1=End1->next;
            End1->d.a=N;
            End1->next=NULL;
        }
        else
        {
            End1->next=new List;
            End1=End1->next;
            End1->d.a=N;
            End1->next=NULL;
            break;
        }
    }
    while(cin>>N)
    {
        End2->next=new List;
        End2=End2->next;
        End2->d.a=N;
        End2->next=NULL;
    }
}
void Delete(List *u1, List *u2)
{
    List *p1=u1, *p2=u2;
    while(p1)
    {
        if(p1->next==p2)
        {
            p1->next=p2->next;
            delete p2;
            p2=p1->next;
            break;
        }
        else
        {
            p1=p1->next;
        }
    }
}
void F(List *u1, List *u2)
{
    List *p1a=u1, *p1b=u1, *p2=u2;
    p1a=p1a->next;
    p1b=p1b->next;
    p2=p2->next;
    while(p1a)
    {
        while(p2)
        {
            if(p1a->d.a==p2->d.a)
            {
                p1a=p1a->next;
                p2=p2->next;
                if(p2->next==NULL && p2->d.a==p1a->d.a)
                {
                    break;
                }
            }
            else
            {
                p1b=p1b->next;
                p1a=p1b;
                p2=u2->next;
            }
        }
        if(p2->next==NULL && p2->d.a==p1a->d.a)
        {
            break;
        }
    }
    p2=u2->next;
    while(p2)
    {
        p1a=p1b->next;
        Delete(u1,p1b);
        p1b=p1a;
        p2=p2->next;
    }
}

void Print(List *u)
{
    List *p=u;
    p=p->next;
    while(p)
    {
        cout<<p->d.a<<" ";
        p=p->next;
    }
}

int main()
{
    List *Begin1=NULL;
    List *Begin2=NULL;
    Init(&Begin1,&Begin2);
    Print(Begin1);
    cout<<endl;
    Print(Begin2);
    cout<<endl;
    F(Begin1, Begin2);
    Print(Begin1);

    return 0;
}
