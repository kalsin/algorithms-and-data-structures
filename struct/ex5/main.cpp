#include <iostream>

using namespace std;

struct Data
{
    int a;
};
struct List
{
    Data d;
    List *next, *prev;
};
void Init(List **Begin)
{
    List *Last=0;
    cout<<"Vvedite spisok:"<<endl;
    int N;
    cin>>N;
    *Begin=new List;
    (*Begin)->d.a=N;
    (*Begin)->next=NULL;
    (*Begin)->prev=NULL;
    Last=(*Begin);
    List *End=*Begin;
    while(cin>>N)
    {
        End->next=new List;
        End=End->next;
        End->d.a=N;
        End->next=NULL;
        End->prev=Last;
        Last=End;
    }
}
void Print(List *u)
{
    List *p=u;
    while(p)
    {
        cout<<p->d.a<<" ";
        p=p->next;
    }
}

void Swap(List *u1, List *u2)
{
    List *p1=u1, *p2=u2;
    Data help=p2->d;
    p2->d=p1->d;
    p1->d=help;
}

void F(List *u)
{
    List *p=u, *End;
    int k=1, m=1;
    while(p)
    {
        End=p;
        p=p->next;
        k++;
    }
    p=u->next;
    End=End->prev;
    if(k>3)
    {
        while(m<k/2)
        {
            Swap(p, End);
            p=p->next;
            End=End->prev;
            m++;
        }
    }
}

int main()
{
    List *Begin=NULL;
    Init(&Begin);
    F(Begin);
    Print(Begin);
    return 0;
}
