#include <iostream>
#include <string>
#include <cstdlib>
#include <iomanip>
using namespace std;

struct Data
{
    string author;
    string name;
    int year;
};
struct List
{
    Data d;
    List *next;
};

void Init(List **Begin, int N)
{
    cout<<"������� ������ ����:"<<endl;
    string S, help;
    int x;

    *Begin=new List;
    (*Begin)->next=NULL;
    List *End=*Begin;

    for(int i=0;i<N;i++)
    {
        End->next=new List;
        End=End->next;
        //cout<<"\n�����: ";
        getline(cin,S);
        End->d.author=S;
        //cout<<"�������� �����: ";
        getline(cin,S);
        End->d.name=S;
        //cout<<"��� �������: ";
        getline(cin,S);
        x=atoi(S.c_str());
        End->d.year=x;
        End->next=NULL;
    }
}

void F(List *u)
{
    int x, y;
    List *p=u;
    p=p->next;
    cout<<"_____________________________________________________________________________________________________";
    cout<<"\n|�����"<<setw(33)<<"|�������� �����"<<setw(62)<<"|��� �������|"<<endl;
    cout<<"|'''''''''''''''''''''''|'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''|'''''''''''|"<<endl;
    while(p)
    {
        if(p->d.year%10==7)
        {
            x=p->d.author.size()+1;
            y=p->d.name.size()+1;
            //cout<<x<<' '<<y<<endl;
            cout<<"|"<<p->d.author<<setw(25-x)<<"|"<<p->d.name<<setw(65-y)<<"|"<<p->d.year<<setw(8)<<"|"<<endl;
        }
        p=p->next;
    }
    cout<<"|_______________________|_______________________________________________________________|___________|";
}

int main()
{
    setlocale(LC_ALL,"Russian");
    string S;
    int N;
    getline(cin,S);
    N=atoi(S.c_str());
    List *Begin=NULL;
    Init(&Begin, N);
    F(Begin);
    return 0;
}
/*
10
Nicholas Takas
JavaScript for professional web-developers
2017
Barbara Oakley
Think like a mathematician.
2016
Demidovich B. P
Collection of tasks and exercises on mathematical analysis
1977
Filippov A. F
Collection of problems on differential equations
2000
Lvovsky S. M
Set and layout in the system LATEX
2007
Nicholas Takas
JavaScript for professional web-developers
2011
Barbara Oakley
Think like a mathematician.
2017
Demidovich B. P
Collection of tasks and exercises on mathematical analysis
1974
Filippov A. F
Collection of problems on differential equations
2007
Lvovsky S. M
Set and layout in the system LATEX
2008
*/
